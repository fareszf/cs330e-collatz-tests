#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve, cycle_length

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)
        
    def test_read_2(self):
        s = "13874 119009\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  13874)
        self.assertEqual(j, 119009)

    def test_read_3(self):
        s = 55
        try:
            i, j = collatz_read(s)  # Invalid input: non-numeric strings
        except AttributeError as e:
            assert str(e) == "'int' object has no attribute 'split'"


    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        try:
            collatz_eval(-10, -1)
        except AssertionError as e:
            assert str(e) == ""

    # -----
    # cycle length
    # -----
    def test_length_1(self):
        val = cycle_length(5, {1:1})
        self.assertEqual(val, 6)
    
    def test_length_2(self):
        val = cycle_length(8285, {1:1})
        self.assertEqual(val, 128)

    def test_length_3(self):
        cache = {1: 1}
        try:
            cycle_length("string", cache)
        except TypeError as e:
            self.assertEqual(str(e), "'>' not supported between instances of 'str' and 'int'")

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 6, 50, 20)
        self.assertEqual(w.getvalue(), "6 50 20\n")

    def test_print_3(self):
        w = None
        try:
            collatz_print(w, 1, 10, 20)
        except AttributeError as e:
            self.assertEqual(str(e), "'NoneType' object has no attribute 'write'")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")
    def test_solve_2(self):
        r = StringIO("1 2\n1 1\n100 100\n100 200\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 2 2\n1 1 1\n100 100 26\n100 200 125\n")
        
    def test_solve_3(self):
        r = ["abc", "def"]
        w = StringIO()
        try:
            collatz_solve(r, w)
        except ValueError as e:
            self.assertEqual(str(e), "invalid literal for int() with base 10: 'abc'")
# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
