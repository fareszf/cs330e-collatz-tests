#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)
    def test_read_2(self):
        s = "1001 2000\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1001)
        self.assertEqual(j, 2000)
    def test_read_3(self):
        s = "50 90\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  50)
        self.assertEqual(j, 90)
    def test_read_4(self):
        s = "4000 5000\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  4000)
        self.assertEqual(j, 5000)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)
    
    def test_eval_5(self):
        v = collatz_eval(1001, 2000)
        self.assertEqual(v, 182)
    
    def test_eval_6(self):
        v = collatz_eval(50, 90)
        self.assertEqual(v, 116)

    def test_eval_7(self):
        v = collatz_eval(4000, 5000)
        self.assertEqual(v, 215)

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")
    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 1001, 2000, 182)
        self.assertEqual(w.getvalue(), "1001 2000 182\n")
    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 50, 90, 116)
        self.assertEqual(w.getvalue(), "50 90 116\n")
    def test_print_4(self):
        w = StringIO()
        collatz_print(w, 4000, 5000, 215)
        self.assertEqual(w.getvalue(), "4000 5000 215\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")
    def test_solve_2(self):
        r = StringIO("1000 2000\n50 90\n4000 5000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1000 2000 182\n50 90 116\n4000 5000 215\n")
    def test_solve_3(self):
        r = StringIO("9000 10000\n8000 8100\n350 450\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "9000 10000 260\n8000 8100 190\n350 450 134\n")


# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
