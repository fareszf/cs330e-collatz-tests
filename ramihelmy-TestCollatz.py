#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve, single_collatz_eval

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read2(self):
        s = "2 9\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  2)
        self.assertEqual(j, 9)

    def test_read3(self):
        s = "3 8\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  3)
        self.assertEqual(j, 8)
   
    def test_read4(self):
        s = "1 999999\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 999999)
    
    def test_read5(self):
        s = "33 88\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  33)
        self.assertEqual(j, 88)
    
    def test_read6(self):
        s = "101 82\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  101)
        self.assertEqual(j, 82)
    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self): 
        v= collatz_eval(10,5)
        expected= collatz_eval(5,10)
        self.assertEqual(v,expected) 

    def test_eval_6(self): 
        self.assertEqual(collatz_eval(5,5),single_collatz_eval(5))

    def test_eval_7(self):
        v = collatz_eval(1000, 900)
        self.assertEqual(v, 174)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 2, 9, 30)
        self.assertEqual(w.getvalue(), "2 9 30\n")
    
    def test_print2(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")
    
    def test_print3(self):
        w = StringIO()
        collatz_print(w, 25, 30, 123)
        self.assertEqual(w.getvalue(), "25 30 123\n")
    
    def test_print4(self):
        w = StringIO()
        collatz_print(w, 1, 999999, 125)
        self.assertEqual(w.getvalue(), "1 999999 125\n")
    
    def test_print5(self):
        w = StringIO()
        collatz_print(w, 26, 2, 13)
        self.assertEqual(w.getvalue(), "26 2 13\n")
    
    def test_print6(self):
        w = StringIO()
        collatz_print(w, 512, 2048, 35)
        self.assertEqual(w.getvalue(), "512 2048 35\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")
    
    def test_solve2(self):
        r = StringIO("1375 1770\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1375 1770 180\n")
    
    def test_solve3(self):
        r = StringIO("188 1311\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "188 1311 182\n")
    
    def test_solve4(self):
        r = StringIO("210 201\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "210 201 89\n")
    
    def test_solve5(self):
        r = StringIO("999999 1\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "999999 1 525\n")
        
    def test_solve6(self):
        r = StringIO("814 1399\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "814 1399 182\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
