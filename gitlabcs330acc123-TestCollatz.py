#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve, collatz_helper, create_collatz_eager

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "101 150\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  101)
        self.assertEqual(j, 150)

    def test_read_3(self):
        s = "201 400\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  201)
        self.assertEqual(j, 400)

    def test_read_4(self):
        s = "543 678\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  543)
        self.assertEqual(j, 678)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125 )

    def test_eval_3(self): 
        v = collatz_eval(1, 1000)
        self.assertEqual(v, 179)

    def test_eval_4(self): 
        v = collatz_eval(10, 1)
        self.assertEqual(v, 20)

    def test_eval_5(self): 
        v = collatz_eval(600000, 750786)
        self.assertEqual(v, 509)

    def test_eval_6(self): 
        v = collatz_eval(650000, 988000)
        self.assertEqual(v, 525)

    def test_eval_7(self): 
        v = collatz_eval(780000, 780100)
        self.assertEqual(v, 300)


    # -----
    # helper
    # -----

    def test_helper_1(self):
        v = collatz_helper(1)
        self.assertEqual(v, 1)

    def test_helper_2(self):
        v = collatz_helper(8)
        self.assertEqual(v, 4) 

    def test_helper_3(self):
        v = collatz_helper(10563)
        self.assertEqual(v, 56) 

    # -----
    # creating eager cache population
    # -----


    def test_create_collatz_eager_1(self):
        v = create_collatz_eager(1,10)
        self.assertEqual(v, {1:1, 2:2, 3:8, 4:3, 5:6, 6:9, 7:17, 8:4, 9:20, 10:7})
    
    def test_create_collatz_eager_2(self):
        v = create_collatz_eager(115,120)
        self.assertEqual(v, {115:34, 116:21, 117:21, 118:34, 119:34, 120:21})
    
    def test_create_collatz_eager_3(self):
        v = create_collatz_eager(601,607)
        self.assertEqual(v, {601:57, 602:18, 603:70, 604:18, 605:18, 606:44, 607:44})
    
    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20978)
        self.assertEqual(w.getvalue(), "1 10 20978\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 900, 675, 174)
        self.assertEqual(w.getvalue(), "900 675 174\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 5, 785694, 0)
        self.assertEqual(w.getvalue(), "5 785694 0\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")
        
    def test_solve_2(self):
        r = StringIO("5 10\n198 200\n201 215\n999 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "5 10 20\n198 200 120\n201 215 102\n999 1000 112\n")
        
    def test_solve_3(self):
        r = StringIO("4 15\n8 988\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "4 15 20\n8 988 179\n")
        
    

# ----
# main
# ----

if __name__ == "__main__": #pragma: no cover
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
