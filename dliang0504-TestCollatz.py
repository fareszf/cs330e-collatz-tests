#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n" 
        i, j = collatz_read(s)
        self.assertEqual(i, 1)
        self.assertEqual(j, 10)
    
    def test_read2(self):
        s = "5 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 5)
        self.assertEqual(j, 10)
    
    def test_read3(self):
        s = "7 14\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 7)
        self.assertEqual(j, 14)
    
    def test_read4(self):
        s = "24 59\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 24)
        self.assertEqual(j, 59)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)
    
    # Adding more tests
         # Additional tests

    def test_eval_5(self):
        """Test with the same start and end value."""
        v = collatz_eval(5, 5)
        self.assertEqual(v, 6)  # The cycle length for 5 is 6.

    def test_eval_6(self):
        """Test the smallest range with a single value."""
        v = collatz_eval(1, 1)
        self.assertEqual(v, 1)  # The cycle length for 1 is 1.

    def test_eval_reverse_order(self):
        """Test with reversed start and end values."""
        v = collatz_eval(10, 1)
        self.assertEqual(v, 20)  # Should handle reverse range gracefully.

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")
    
    def test_print1(self):
        w = StringIO()
        collatz_print(w, 163, 648, 144)
        self.assertEqual(w.getvalue(), "163 648 144\n")

    def test_print2(self):
        w = StringIO()
        collatz_print(w, 464, 776, 171)
        self.assertEqual(w.getvalue(), "464 776 171\n")

    def test_print3(self):
        w = StringIO()
        collatz_print(w, 338, 860, 171)
        self.assertEqual(w.getvalue(), "338 860 171\n")
    
    def test_print4(self):
        w = StringIO()
        collatz_print(w, 453, 474, 129)
        self.assertEqual(w.getvalue(), "453 474 129\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")
        
    def test_solve1(self):
        r = StringIO("163 648\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "163 648 144\n")
    
    def test_solve2(self):
        r = StringIO("464 776\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "464 776 171\n")
    
    def test_solve3(self):
        r = StringIO("338 860\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "338 860 171\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
