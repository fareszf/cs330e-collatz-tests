#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, cycle_length, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)
        
    def test_read_2(self):
        s = "3 21\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  3)
        self.assertEqual(j, 21)
        
    def test_read_3(self):
        s = "28 999999\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  28)
        self.assertEqual(j, 999999)
    # -----
    # cycle_length
    # -----

    def test_cycle_length_1(self):
        v = cycle_length(5)
        self.assertEqual(v, 6)

    def test_cycle_length_2(self):
        v = cycle_length(1000)
        self.assertEqual(v, 112)

    def test_cycle_length_3(self):
        v = cycle_length(384732)
        self.assertEqual(v, 100)
    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)
        
    def test_eval_5(self):
        v = collatz_eval(7851, 1374)
        self.assertEqual(v, 262 )
    
    def test_eval_6(self):
        v = collatz_eval(81932, 65327)
        self.assertEqual(v, 351)
    
    def test_eval_7(self):
        v = collatz_eval(156372, 275839)
        self.assertEqual(v, 443)
        
    def test_eval_8(self):
        v = collatz_eval(1001, 3000)
        self.assertEqual(v, 217)
    
    def test_eval_9(self):
        v = collatz_eval(999001, 999999)
        self.assertEqual(v, 396)

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")
        
    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 524054, 633116, 509)
        self.assertEqual(w.getvalue(), "524054 633116 509\n")
        
    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 89319, 996504 , 525)
        self.assertEqual(w.getvalue(), "89319 996504 525\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")
        
    def test_solve_2(self):
        r = StringIO("3669 5611\n7774 9280\n1154 3438\n300 2547\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "3669 5611 238\n7774 9280 260\n1154 3438 217\n300 2547 209\n")
    
    def test_solve_3(self):
        r = StringIO("60 500\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(w.getvalue(), "60 500 144\n")
# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
