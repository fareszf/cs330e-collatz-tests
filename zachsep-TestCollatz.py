#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        # test with typical valid input
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)
    
    def test_read_2(self):
        # test with identical numbers
        s = "5 5\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 5)
        self.assertEqual(j, 5)
        
    def test_read_3(self): 
        # test of large values
        s = " 1000000 2000000\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1000000)
        self.assertEqual(j, 2000000)
    
    def test_read_4(self): 
        # where i is greater than j
        s = " 125 7\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 125)
        self.assertEqual(j, 7)
    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)
    
    def test_eval_2(self):
        v = collatz_eval(329, 979)
        self.assertEqual(v, 179)

    def test_eval_3(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_4(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_5(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    # -----
    # print
    # -----

    def test_print_1(self):
        # testing with the same start and end values
        w = StringIO()
        collatz_print(w, 7, 7, 17)
        self.assertEqual(w.getvalue(), "7 7 17\n")
    
    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")
        
    def test_print_3(self):
        # testing large values
        w = StringIO()
        collatz_print(w, 13333, 93495834958, 454455)
        self.assertEqual(w.getvalue(), "13333 93495834958 454455\n")
        
    # -----
    # solve
    # -----

    def test_solve_1(self):
        # testing multiple lines of input
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_2(self):
        r = StringIO("1 10\n10 1\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n10 1 20\n")
    
    def test_solve_3(self):
        # testing with larger range
        r = StringIO("1 100000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(),"1 100000 351\n")

# ----
# main
# ----

if __name__ == "__main__": # pragma: no cover
    main()

""" #pragma: no coverpytho
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
