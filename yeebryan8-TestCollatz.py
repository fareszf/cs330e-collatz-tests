#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve, collatz_calc

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "999 10101\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  999)
        self.assertEqual(j, 10101)

    def test_read_3(self):
        s = "8281 2230\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  8281)
        self.assertEqual(j, 2230)

    def test_read_4(self):
        s = "99999 999999\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  99999)
        self.assertEqual(j, 999999)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(1, 9999)
        self.assertEqual(v, 262)

    def test_eval_6(self):
        v = collatz_eval(2000, 40)
        self.assertEqual(v, 182)

    def test_eval_7(self):
        v = collatz_eval(50, 50)
        self.assertEqual(v, 25)

    # -----
    # calc
    # -----
    
    def test_calc_1(self):
        v = collatz_calc(10)
        self.assertEqual(v, 7)

    def test_calc_2(self):
        v = collatz_calc(1)
        self.assertEqual(v, 1)

    def test_calc_3(self):
        v = collatz_calc(999999)
        self.assertEqual(v, 259)

    def test_calc_4(self):
        v = collatz_calc(100)
        self.assertEqual(v, 26)

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 100, 200, 125)
        self.assertEqual(w.getvalue(), "100 200 125\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 900, 1000, 174)
        self.assertEqual(w.getvalue(), "900 1000 174\n")
    
    def test_print_4(self):
        w = StringIO()
        collatz_print(w, 323, 525, 144)
        self.assertEqual(w.getvalue(), "323 525 144\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")
        
    def test_solve_2(self):
        r = StringIO("1 1\n1 2\n2 1\n2 3\n3 2\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 1 1\n1 2 2\n2 1 2\n2 3 8\n3 2 8\n")
        
    def test_solve_3(self):
        r = StringIO("323 525\n525 323\n666 999\n999 666\n1001 1002\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "323 525 144\n525 323 144\n666 999 179\n999 666 179\n1001 1002 143\n")
    
    def test_solve_4(self):
        r = StringIO("4884 3993\n987 123\n250 750\n99999 22222\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "4884 3993 215\n987 123 179\n250 750 171\n99999 22222 351\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
