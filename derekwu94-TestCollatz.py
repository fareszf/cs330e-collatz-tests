#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve, cycle_length

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_negative_values(self):
        s = "-3 -1\n"
        i, j = collatz_read(s)
        self.assertEqual(i, -3)
        self.assertEqual(j, -1)

    def test_read_extra_spaces(self):
        s = "  2 5  \n"
        i, j = collatz_read(s)
        self.assertEqual(i, 2)
        self.assertEqual(j, 5)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)
        
    def test_eval_5(self):
        v = collatz_eval(111, 112)
        self.assertEqual(v, 70)
    
    def test_eval_6(self):
        v = collatz_eval(999998, 999999)
        self.assertEqual(v, 259)
    
    def test_eval_7(self):
        v = collatz_eval(1, 999999)
        self.assertEqual(v, 525)
    
    def test_eval_8(self):
        with self.assertRaises(AssertionError):
            collatz_eval(-1, 999999)
    
    def test_eval_9(self):
        v = collatz_eval(1, 1000)
        self.assertEqual(v, 179)

    def test_eval_10(self):
        v = collatz_eval(2000, 3500)
        self.assertEqual(v, 217)

    def test_eval_11(self):
        v = collatz_eval(1500, 2000)
        self.assertEqual(v, 180)

    def test_eval_12(self):
        v = collatz_eval(1234, 5678)
        self.assertEqual(v, 238)

    def test_eval_13(self):
        v = collatz_eval(1000, 2000)
        self.assertEqual(v, 182)

    def test_eval_14(self):
        v = collatz_eval(1001, 2000)
        self.assertEqual(v, 182)

    def test_eval_15(self):
        v = collatz_eval(2000, 3001)
        self.assertEqual(v, 217)

    def test_eval_16(self):
        v = collatz_eval(500, 500)
        self.assertEqual(v, 111)

    def test_eval_17(self):
        v = collatz_eval(1500, 3000)
        self.assertEqual(v, 217)
    
    def test_eval_18(self):
        v = collatz_eval(1500, 2500)
        self.assertEqual(v, 209)
    
    def test_eval_19(self):
        v = cycle_length(1000, 1000)
        self.assertEqual(v, 182)

        
    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")
        
    def test_print_zero_values(self):
        w = StringIO()
        collatz_print(w, 0, 0, 0)
        self.assertEqual(w.getvalue(), "0 0 0\n")

    def test_print_single_value(self):
        w = StringIO()
        collatz_print(w, 7, 7, 14)
        self.assertEqual(w.getvalue(), "7 7 14\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")
    
    def test_solve_single(self):
        r = StringIO("5 5\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(w.getvalue(), "5 5 6\n")  

    def test_solve_empty_input(self):
        r = StringIO("")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(w.getvalue(), "")
        
    def test_solve_error(self):
        with self.assertRaises(AssertionError):
            r = StringIO("100 -100\n")
            w = StringIO()
            collatz_solve(r, w)

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
