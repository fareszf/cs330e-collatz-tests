#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
   # ----
   # read
   # ----

   def test_read_1(self):
       s = "1 10\n"
       i, j = collatz_read(s)
       self.assertEqual(i,  1)
       self.assertEqual(j, 10)

   def test_read_2(self):
       s = "45723 67894\n"
       i, j = collatz_read(s)
       self.assertEqual(i, 45723)
       self.assertEqual(j, 67894)

   def test_read_3(self):
       s = "345703 567895\n"
       i, j = collatz_read(s)
       self.assertEqual(i, 345703)
       self.assertEqual(j, 567895)

   # ----
   # eval
   # ----

   def test_eval_1(self):
       v = collatz_eval(1, 10)
       self.assertEqual(v, 20)

   def test_eval_2(self):
       v = collatz_eval(100, 200)
       self.assertEqual(v, 125)

   def test_eval_3(self):
       v = collatz_eval(201, 210)
       self.assertEqual(v, 89)

   def test_eval_4(self):
       v = collatz_eval(900, 1000)
       self.assertEqual(v, 174)

   def test_eval_int(self):
       v = collatz_eval(10000, 20000)
       self.assertEqual(v, int(279))

   def test_eval_negative(self):
       v = collatz_eval(-10000, -1)
       self.assertEqual(v, 0)

   def test_eval_zero(self):
       v = collatz_eval(0, 0)
       self.assertEqual(v, 0)

   def test_eval_reverse_range(self):
       v = collatz_eval(200, 100)
       self.assertEqual(v, 125)  # Since the function swaps i and j

   def test_eval_mixed_negative_positive(self):
       v = collatz_eval(-100, 100)
       self.assertEqual(v, 0)  # Mixed negative-positive input should return 0

   def test_eval_large_range(self):
       v = collatz_eval(1, 1000000)
       self.assertGreaterEqual(v, 500)  # Expected large result for range

   # -----
   # print
   # -----

   def test_print_1(self):
       w = StringIO()
       collatz_print(w, 1, 10, 20)
       self.assertEqual(w.getvalue(), "1 10 20\n")

   def test_print_2(self):
       w = StringIO()
       collatz_print(w, 10000, 20000, 279)
       self.assertEqual(w.getvalue(), "10000 20000 279\n")

   def test_print_3(self):
       w = StringIO()
       collatz_print(w, 200000, 210000, 373)
       self.assertEqual(w.getvalue(), "200000 210000 373\n")

   # -----
   # solve
   # -----

   def test_solve_1(self):
       r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
       w = StringIO()
       collatz_solve(r, w)
       self.assertEqual(
           w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

   def test_solve_2(self):
       r = StringIO("30000 40000\n40000 50000\n50000 60000\n60000 70000\n")
       w = StringIO()
       collatz_solve(r, w)
       self.assertEqual(
           w.getvalue(), "30000 40000 324\n40000 50000 314\n50000 60000 340\n60000 70000 335\n")

   def test_solve_3(self):
       r = StringIO("200000 210000\n300000 310000\n400000 410000\n500000 510000\n")
       w = StringIO()
       collatz_solve(r, w)
       self.assertEqual(
           w.getvalue(), "200000 210000 373\n300000 310000 371\n400000 410000 405\n500000 510000 426\n")

   def test_solve_mixed_input(self):
       r = StringIO("0 10\n-10 -1\n100 200\n")
       w = StringIO()
       collatz_solve(r, w)
       self.assertEqual(w.getvalue(), "0 10 0\n-10 -1 0\n100 200 125\n")




# ----
# main
# ----
if __name__ == "__main__":
   main()


""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
