#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read2(self):
        s = "2342 1540\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  2342)
        self.assertEqual(j, 1540)

    def test_read3(self):
        s = "9740 8791\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  9740)
        self.assertEqual(j, 8791)

    def test_read4(self):
        s = "64 64\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  64)
        self.assertEqual(j, 64)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(2342, 1540)
        self.assertEqual(v, 183)
    
    def test_eval_6(self):
        v = collatz_eval(1, 1)
        self.assertEqual(v, 1)

    def test_eval_7(self):
        v = collatz_eval(64, 64)
        self.assertEqual(v, 7)

    def test_eval_8(self):
        v = collatz_eval(1245, 907540)
        self.assertEqual(v, 525)

    def test_eval_9(self):
        v = collatz_eval(2000, 3000)
        self.assertEqual(v, 217)

    def test_eval_10(self):
        v = collatz_eval(1, 953849)
        self.assertEqual(v, 525)

    # -----
    # print
    # -----

    def test_print1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print2(self):
        w = StringIO()
        collatz_print(w, 50, 20, 112)
        self.assertEqual(w.getvalue(), "50 20 112\n")

    def test_print3(self):
        w = StringIO()
        collatz_print(w, 2342, 1540, 183)
        self.assertEqual(w.getvalue(), "2342 1540 183\n")

    def test_print4(self):
        w = StringIO()
        collatz_print(w, 1, 1, 1)
        self.assertEqual(w.getvalue(), "1 1 1\n")

    # -----
    # solve
    # -----

    def test_solve1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")
        
    def test_solve2(self):
        r = StringIO("1265 2097\n1707 1121\n1495 2628\n2637 1929\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1265 2097 180\n1707 1121 182\n1495 2628 209\n2637 1929 209\n")
        
    def test_solve3(self):
        r = StringIO("1772 617\n432 4371\n4649 3681\n64 64\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1772 617 182\n432 4371 238\n4649 3681 238\n64 64 7\n")
    
    def test_solve4(self):
        r = StringIO("44 1183\n1181 346\n747 588\n992 742\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "44 1183 182\n1181 346 182\n747 588 171\n992 742 179\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
