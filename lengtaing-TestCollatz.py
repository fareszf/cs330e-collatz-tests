#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve, cycle_length, eager_cache


# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    """
    Defined class for Collatz.py unit tests.
    """
    # ----
    # read
    # ----

    def test_read_1(self):
        """
        Unit test for the collatz_read function with a normal interval input.
        """
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        """
        Unit test for the collatz_read function with a reversed interval input.
        """
        s = "10 1\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  10)
        self.assertEqual(j, 1)

    def test_read_3(self):
        """
        Unit test for the collatz_read function when i == j input.
        """
        s = "999999 999999\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 999999)
        self.assertEqual(j, 999999)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        """
        Unit test for the collatz_eval function with a normal interval input.
        """
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        """
        Unit test for the collatz_eval function with a normal interval input.
        """
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        """
        Unit test for the collatz_eval function with a normal interval input.
        """
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        """
        Unit test for the collatz_eval function with a normal interval input.
        """
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_reversed_input(self):
        """
        Unit test for the collatz_eval function with a reversed interval input.
        """
        v = collatz_eval(10, 1)
        self.assertEqual(v, 20)

    def test_eval_min_input(self):
        """
        Edge case unit test for the collatz_eval function for the min allowable input.
        """
        v = collatz_eval(1, 1)
        self.assertEqual(v, 1)

    def test_eval_max_input(self):
        """
        Edge case unit test for the collatz_eval function for the max allowable input.
        """
        v = collatz_eval(999999, 999999)
        self.assertEqual(v, 259)

    def test_eval_high_value_different_range(self):
        """
        Unit test for the collatz_eval function with a higher range interval input.
        """
        v = collatz_eval(900000, 999999)
        self.assertEqual(v, 507)

    # ------------
    # cycle length
    # ------------

    def test_cycle(self):
        """
        Edge case unit test for the cycle_length function for min allowable input.
        """
        self.assertEqual(cycle_length(1), 1)

    def test_cycle2(self):
        """
        Unit test for the cycle_length function with a regular input within the range.
        """
        self.assertEqual(cycle_length(10), 7)

    def test_cycle_max_input(self):
        """
        Edge case unit test for the cycle_length function for max allowable input.
        """
        self.assertEqual(cycle_length(999999), 259)

    # -----
    # print
    # -----

    def test_print(self):
        """
        Unit test for the collatz_print function with a normal interval input.
        """
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_reversed_input(self):
        """
        Unit test for the collatz_print function with a reversed interval input.
        """
        w = StringIO()
        collatz_print(w, 20, 10, 1)
        self.assertEqual(w.getvalue(), "20 10 1\n")

    def test_print_multiple_calls(self):
        """
        Unit test for the collatz_print function with multiple function calls.
        """
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        collatz_print(w, 5, 5, 5)
        self.assertEqual(w.getvalue(), "1 10 20\n5 5 5\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        """
        Unit test for the collatz_solve function with multiple normal intervals input.
        """
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_max_input(self):
        """
        Edge case unit test for the collatz_solve function with max allowable input.
        """
        r = StringIO("999999 999999\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(w.getvalue(), "999999 999999 259\n")

    def test_solve_reversed_input(self):
        """
        Unit test for the collatz_solve function with a reversed interval input.
        """
        r = StringIO("10 1\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(w.getvalue(), "10 1 20\n")

# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
....................
----------------------------------------------------------------------
Ran 20 tests in 7.166s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
....................
----------------------------------------------------------------------
Ran 20 tests in 7.166s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          63      0     28      0   100%
TestCollatz.py      79      0      0      0   100%
------------------------------------------------------------
TOTAL              142      0     28      0   100%
"""
